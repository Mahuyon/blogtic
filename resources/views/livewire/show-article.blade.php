<!-- <div>

<div class="max-w-2xl px-8 py-4 mx-auto bg-white rounded-lg shadow-md dark:bg-gray-800" style="cursor: auto;">
  <div class="flex items-center justify-between">
    <span class="text-sm font-light text-gray-600 dark:text-gray-400">Jan 15, 2022</span> 
    <a class="px-3 py-1 text-sm font-bold text-gray-100 transition-colors duration-200 transform bg-gray-600 rounded cursor-pointer hover:bg-gray-500">JavaScript</a>
  </div> 
  <div class="mt-2">
    <a href="https://stackdiary.com/" class="text-2xl font-bold text-gray-700 dark:text-white hover:text-gray-600 dark:hover:text-gray-200 hover:underline">How to sanitiz array() in JS</a> 
    <p class="mt-2 text-gray-600 dark:text-gray-300">Dui urna vehicula tincidunt pretium consequat luctus mi, platea fermentum conubia tempus ac orci. Pellentesque dictum malesuada cubilia faucibus dignissim mi nascetur senectus, augue ad libero efficitur dolor duis lobortis, non etiam sociosqu.</p>
  </div> 
  <div class="flex items-center justify-between mt-4">
    <a href="#" class="text-blue-600 dark:text-blue-400 hover:underline">Read more ⟶</a> 
    <div class="flex items-center">
      <img src="https://stackdiary.com/140x100.png" alt="Author Photo" class="hidden object-cover w-10 h-10 mx-4 rounded-full sm:block"> 
      <a class="font-bold text-gray-700 cursor-pointer dark:text-gray-200">John Doe</a>
    </div>
  </div>
</div>
</div>
 -->
<!-- component -->
<div class="py-16 bg-gradient-to-br from-green-50 to-cyan-100">  
  <div class="container m-auto px-6 text-gray-600 md:px-12 xl:px-6">
      <div class="mb-12 space-y-2 text-center">
        <span class="block w-max mx-auto px-3 py-1.5 border border-green-200 rounded-full bg-green-100 text-green-600">Blog</span>
        <h2 class="text-2xl text-cyan-900 font-bold md:text-4xl">LATOMV TIC ABC</h2>
        <p class="lg:w-6/12 lg:mx-auto">Quam hic dolore cumque voluptate rerum beatae et quae, tempore sunt, debitis dolorum officia aliquid explicabo? Excepturi, voluptate?</p>
      </div>

      <div class="grid gap-12 lg:grid-cols-4">
      @foreach ($articles as $article)
          <div class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                  <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-900 dark:text-white">{{ $article->title_article }}</h5>
              </a>
              <p class="mb-3 font-normal text-gray-500 dark:text-gray-400">{{ substr($article->content_article, 0, 60).'...' }}</p>
              <a href="{{ route('detailArticle', ['id' => $article->id]) }}" class="inline-flex items-center text-blue-600 hover:underline">
                  Read more
                  <svg class="w-5 h-5 ml-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z"></path><path d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z"></path></svg>
              </a>
          </div>
      @endforeach
        </div>
</div>

</div>
