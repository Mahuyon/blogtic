<div>

<!-- component -->
<div class="mt-6 bg-gray-50">
                  <div class=" px-10 py-6 mx-auto">
                        
                        <!--author-->
                     <div class="max-w-6xl px-10 py-6 mx-auto bg-gray-50">

		                <!--post categories-->
		                
                        <div class="mt-2">
                        	<!--post heading-->
                        	<a href="#"
                                class="sm:text-3xl md:text-3xl lg:text-3xl xl:text-4xl font-bold text-purple-500  hover:underline">
                                {{ $oneArticle->title_article }}</a>

                                <!--post views-->
                                <div class="flex justify-start items-center mt-2">
                                	<p class="text-sm text-green-500 font-bold bg-gray-100 rounded-full py-2 px-2 hover:text-red-500">3000</p>
                                	<p class="text-sm text-gray-400 font-bold ml-5">Views</p>
                                
                                </div>

                                <!--author avator-->
                               <div class="font-light text-gray-600">
	                              
	                        		<a href="#" class="flex items-center mt-6 mb-6">
	                                    <h1 class="font-bold text-gray-700 hover:underline">By James Amos</h1>
	                                </a>
                        	  </div>
                       </div>
                       
                       <!--end post header-->
                        	<!--post content-->
                 <div class="max-w-4xl px-10  mx-auto text-2xl text-gray-700 mt-4 rounded bg-gray-100">

                 	<!--content body-->
                 	<div>
                        	<p class="mt-2 p-8">
                            {{ $oneArticle->content_article }}</p>
                     </div>

                     

				        </div>
				    </div>


				    <!--form form comments-->
                     
			 
                    <div class="max-w-4xl py-16 xl:px-8 flex justify-center mx-auto">
						        
				<div class="w-full mt-16 md:mt-0 ">
				   <form class="relative z-10 h-auto p-8 py-10 overflow-hidden bg-white border-b-2 border-gray-300 rounded-lg shadow-2xl px-7">
					   <h3 class="mb-6 text-2xl font-medium text-center">Write a comment</h3>
					   <textarea type="text" name="comment" class="w-full px-4 py-3 mb-4 border border-2 border-transparent border-gray-200 rounded-lg focus:ring focus:ring-blue-500 focus:outline-none" placeholder="Write your comment" rows="5" cols="33"></textarea>
					   <input type="submit" value="Submit comment" name="submit" class=" text-white px-4 py-3 bg-blue-500  rounded-lg">
				   </form>
			   </div>
		    </div>


				<!--comments-->
				
		<div class="max-w-4xl px-10 py-16 mx-auto bg-gray-100  bg-white min-w-screen animation-fade animation-delay  px-0 px-8 mx-auto sm:px-12 xl:px-5">
		        
		        <p class="mt-1 text-2xl font-bold text-left text-gray-800 sm:mx-6 sm:text-2xl md:text-3xl lg:text-4xl sm:text-center sm:mx-0">
		            All comments on this post
		        </p>
		        <!--comment 1-->
		        <div class="flex  items-center w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3">

		        	<a href="#" class="flex items-center mt-6 mb-6 mr-6"><img src="https://avatars.githubusercontent.com/u/71964085?v=4" alt="avatar" class="hidden object-cover w-14 h-14 mx-4 rounded-full sm:block">
			        </a>

		            <div><h3 class="text-lg font-bold text-purple-500 sm:text-xl md:text-2xl">By James Amos</h3>
		            	<p class="text-sm font-bold text-gray-300">August 22,2021</p>
		            	<p class="mt-2 text-base text-gray-600 sm:text-lg md:text-normal">
		               Please help with how you did the migrations for the blog database fields.I tried mine using exactly what you instructed but its not working!!.</p>
		            </div>
		            
		        </div>
		        <!--comment 2-->
		        <div class="flex  items-center w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3">

		        	<a href="#" class="flex items-center mt-6 mb-6 mr-6"><img src="https://avatars.githubusercontent.com/u/71964085?v=4" alt="avatar" class="hidden object-cover w-14 h-14 mx-4 rounded-full sm:block">
			        </a>

		            <div><h3 class="text-lg font-bold text-purple-500 sm:text-xl md:text-2xl">By James Amos</h3>
		            	<p class="text-sm font-bold text-gray-300">August 22,2021</p>
		            	<p class="mt-2 text-base text-gray-600 sm:text-lg md:text-normal">
		                Especially I dont understand the concepts of multiple models.What really is the difference between the blog model and blogApp model? Am stuck</p>
		            </div>
		        </div>

		    </div>
		  </div>
	  </div>
    </div>

</div>
