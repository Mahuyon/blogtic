
<!-- <div class="flex justify-center mt-100">
  <div class="block p-6 rounded-lg shadow-lg bg-white max-w-sm">
    <h5 class="text-gray-900 text-xl leading-tight font-medium mb-2">Login</h5>
    <div>
        <div><label for="">Email: </label></div>
            <div class="md:w-2/3">
                <input class="md:w-2/3 bg-gray-300" type="email" name="email" id="">
            </div>

        <div><label for="">Password: </label></div>
            <div class="md:w-2/3">
                <input class="md:w-2/3" type="password" name="password" id="">
            </div>
        </div>
    </div>
    <button type="button" class=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Login</button>
  </div>
</div>
 -->
 <div class="flex pt-50 py-15 justify-center mb-50 mt-100">
 <div class="w-full max-w-xs m-50">
  <form wire:submit.prevent="submit" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" name="logintop">
    <div class="mb-4">
      <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
        Email
      </label>
      <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" wire:model="user.email" type="email" placeholder="Your email">
    </div>
    <div class="mb-6">
      <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
        Password
      </label>
      <input class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" wire:model="user.password" type="password" placeholder="******************">
    </div>
    <div class="flex items-center justify-between">
      <input class="bg-blue-500 button hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        type="submit" value="Sign In" />
      <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#">
        Forgot Password?
      </a>
    </div>
  </form>
  <p class="text-center text-gray-500 text-xs">
    &copy;2023 LATOMV. All rights reserved.
  </p>
</div>
</div>