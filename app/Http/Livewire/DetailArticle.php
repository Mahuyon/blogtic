<?php

namespace App\Http\Livewire;

use Livewire\Component;
use \App\Models\Article;

class DetailArticle extends Component
{
    public $oneArticle;

    public function mount($id)
    {
        $this->oneArticle = Article::find($id);
        
    }
    

    public function render()
    {
        return view('livewire.detail-article');
    }
}
