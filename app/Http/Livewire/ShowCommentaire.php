<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowCommentaire extends Component
{
    public function render()
    {
        return view('livewire.show-commentaire');
    }
}
