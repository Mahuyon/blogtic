<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowArticle extends Component
{
    public function render()
    {
        $articles = \App\Models\Article::all();
        // return view('livewire.show-article');
        return view('livewire.show-article', ['articles'=>$articles]);

    }
}
