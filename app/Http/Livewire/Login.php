<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Login extends Component
{
    public function submit()
    {
        /* $validatedData = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email',
            'body' => 'required',
        ]);
   
        Contact::create($validatedData); */
   
        return redirect()->to('/blog');
    }

    public function render()
    {
        return view('livewire.login')->layout("layouts.auth");
    }
}
