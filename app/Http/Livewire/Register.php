<?php

namespace App\Http\Livewire;
//namespace App\Models;


use Livewire\Component;

class Register extends Component
{

    public $name;
    public $email;
 
    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
        'password' => 'cpassword',
    ];
 
    public function submit()
    {
        $this->validate();
 
        // Execution doesn't reach here if validation fails.
 
        \App\Models\User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->email),
            'role_id' => $this->role_id,

        ]);
        
        return redirect()->to('/login');
    }

    public function render()
    {
        $role = \App\Models\Role::where('name_role', '!=' , 'admin')->get();
        return view('livewire.register', ['role'=>$role])->layout("layouts.auth");
    }
}
