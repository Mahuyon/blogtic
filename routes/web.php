<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\ShowCommentaire;
use App\Http\Livewire\ShowArticle;
use App\Http\Livewire\DetailArticle;
use App\Http\Livewire\Login;
use App\Http\Livewire\Register;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',Login::class);
Route::get('/register',Register::class);
Route::get('/lordhome',ShowCommentaire::class);
Route::get('/blog',ShowArticle::class);
Route::get('/blog/{id}',DetailArticle::class)->name('detailArticle');
Route::get('/', function () {
    return view('welcome');
});
